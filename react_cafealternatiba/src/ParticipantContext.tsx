import { createContext, useState, useEffect } from "react";

//create context
export const ParticipantsContext = createContext({});

const ParticipantsContextProvider = ({ children }: any) => {
    const [participants, setParticipants] = useState([""]);

    useEffect(() => {
        fetch("http://127.0.0.1:8000/api/participants")
            .then((response) => response.json())
            .then((participant) => {
                setParticipants(participant["hydra:member"]);
            });
    }, []);

    return (
        <ParticipantsContext.Provider
            value={{
                participants,
                setParticipants,
            }}
        >
            {children}
        </ParticipantsContext.Provider>
    );
};

export default ParticipantsContextProvider;
