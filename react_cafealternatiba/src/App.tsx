import { BrowserRouter, Routes, Route } from "react-router-dom";
import "./App.css";
import Accueil from "./pages/Accueil";
import Evenement from "./pages/Evenement";
import Form from "./pages/Form";

export default function App() {
    return (
        <BrowserRouter>
            <Routes>
                <Route path={"/"} element={<Accueil />}></Route>
                <Route path={"/evenement"} element={<Evenement />}></Route>
                <Route path={"/ajout"} element={<Form />}></Route>
            </Routes>
        </BrowserRouter>
    );
}
