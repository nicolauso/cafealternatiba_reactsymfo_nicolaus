import { createContext, useState, useEffect } from "react";

//create context
export const EvenementsContext = createContext({});

const EvenementsContextProvider = ({ children }: any) => {
    const [evenements, setEvenements] = useState([""]);

    useEffect(() => {
        fetch("http://127.0.0.1:8000/api/evenements")
            .then((response) => response.json())
            .then((evenement) => {
                setEvenements(evenement["hydra:member"]);
            });
    }, []);

    return (
        <EvenementsContext.Provider
            value={{
                evenements,
                setEvenements,
            }}
        >
            {children}
        </EvenementsContext.Provider>
    );
};

export default EvenementsContextProvider;
