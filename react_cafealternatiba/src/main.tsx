import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.tsx";
import "./index.css";
import EvenementsContextProvider from "./EvenementsContext.tsx";
import ParticipantsContextProvider from "./ParticipantContext.tsx";

// FETCH ALL EVENEMENTS

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
    // WARNING : ENSER À METTRE LE PROVIDER SUR LE MAIN
    <EvenementsContextProvider>
        <ParticipantsContextProvider>
            <React.StrictMode>
                <App />
            </React.StrictMode>
        </ParticipantsContextProvider>
    </EvenementsContextProvider>
);
