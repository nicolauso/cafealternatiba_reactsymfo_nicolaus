import { useContext, useState, useEffect } from "react";
import { EvenementsContext } from "../EvenementsContext";
import { ParticipantsContext } from "../ParticipantContext";

export default function OneEvent() {
    const { evenements }: any = useContext(EvenementsContext);
    const { participants }: any = useContext(ParticipantsContext);
    const [participant, setParticipant]: any = useState({});
    const [eventId, setEventId] = useState(null);
    const [dataFormParticipant, setDataFormParticipant] = useState({
        nom: "string",
        prenom: "string",
        telephone: 0,
        id_evenement: "",
    });
    console.log(eventId);

    const handleChange = (e) => {
        const { name, value } = e.target;
        setDataFormParticipant((prev) => {
            return {
                ...prev,
                [name]: value,
            };
        });
    };
    const handleSubmit = (e) => {
        e.preventDefault();

        fetch("http://127.0.0.1:8000/api/participants", {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },

            body: JSON.stringify(dataFormParticipant),
        }).then(() => {
            alert("Votre participation à l'événement a bien été enregistré");
            location.assign("http://localhost:5173/evenement");
        });
    };

    function updateEventId(id: any) {
        // let path = URL("/api/evenements/${id}");
        setEventId(id);
    }

    console.log(dataFormParticipant);

    useEffect(() => {}, [evenements][participants]);
    return (
        <div>
            {evenements?.map((evenement: any, index: number) => {
                return (
                    <div key={index}>
                        <div className="card mb-3 p-3">
                            <div className="row g-0">
                                <div className="col-md-4 d-flex align-items-center">
                                    <img
                                        src="/images/activite1.jpg"
                                        className="img-fluid rounded-start"
                                        alt="..."
                                    />
                                </div>
                                <div className="col-md-8">
                                    <div className="card-body">
                                        <h5 className="card-title">
                                            {evenement.intitule}
                                        </h5>
                                        {/* <h6 className="card-text">
                                            <small className="">Date :</small>
                                        </h6> */}
                                        <p className="card-text">
                                            {evenement.description}
                                        </p>
                                        <p className="card-text">
                                            <small className="text-muted">
                                                Thème : {evenement.theme}
                                            </small>
                                        </p>
                                        {/* <!-- Button trigger modal --> */}
                                        <button
                                            onClick={() =>
                                                updateEventId(evenement.id)
                                            }
                                            type="button"
                                            className="btn btn-primary mt-5"
                                            data-bs-toggle="modal"
                                            data-bs-target="#exampleModal"
                                        >
                                            Participer
                                        </button>

                                        {/* <!--MODALE--> */}
                                        <div
                                            className="modal fade"
                                            id="exampleModal"
                                            tabIndex="-1"
                                            aria-labelledby="exampleModalLabel"
                                            aria-hidden="true"
                                        >
                                            <div className="modal-dialog">
                                                <div className="modal-content">
                                                    <div className="modal-header">
                                                        <h2
                                                            className=" "
                                                            id="exampleModalLabel"
                                                        >
                                                            S'inscrire à l'
                                                            {evenement.intitule}
                                                        </h2>
                                                        <button
                                                            type="button"
                                                            className="btn-close"
                                                            data-bs-dismiss="modal"
                                                            aria-label="Close"
                                                        ></button>
                                                    </div>
                                                    <div className="modal-body">
                                                        <h4>Modalités</h4>
                                                        <ul>
                                                            <li className="mt-3 py-2">
                                                                {" "}
                                                                <b>
                                                                    Description
                                                                </b>
                                                                <br />
                                                                {
                                                                    evenement.description
                                                                }
                                                            </li>
                                                            <li className="py-2">
                                                                {" "}
                                                                <b>Thème</b> :
                                                                &nbsp;
                                                                {
                                                                    evenement.theme
                                                                }
                                                            </li>
                                                            <li className="py-2">
                                                                <b>Date</b> :
                                                                &nbsp;{" "}
                                                            </li>
                                                        </ul>
                                                        <h4 className="py-3">
                                                            Vos coordonnées
                                                        </h4>
                                                        {/* FORM */}
                                                        <form action="">
                                                            <div className="mb-3">
                                                                <label
                                                                    htmlFor="exampleInputNom"
                                                                    className="form-label"
                                                                >
                                                                    Nom
                                                                </label>
                                                                <input
                                                                    name="nom"
                                                                    onChange={
                                                                        handleChange
                                                                    }
                                                                    value={
                                                                        participant.nom
                                                                    }
                                                                    type="nom"
                                                                    className="form-control"
                                                                    id="exampleInputNom"
                                                                    aria-describedby="nom"
                                                                />
                                                            </div>
                                                            <div className="mb-3">
                                                                <label
                                                                    htmlFor="exampleInputPrenom"
                                                                    className="form-label"
                                                                >
                                                                    Prénom
                                                                </label>
                                                                <input
                                                                    name="prenom"
                                                                    onChange={
                                                                        handleChange
                                                                    }
                                                                    value={
                                                                        participant.prenom
                                                                    }
                                                                    type="prenom"
                                                                    className="form-control"
                                                                    id="exampleInputPrenom"
                                                                    aria-describedby="prenom"
                                                                />
                                                            </div>
                                                            <div className="mb-3">
                                                                <label
                                                                    htmlFor="exampleInputTel"
                                                                    className="form-label"
                                                                >
                                                                    Téléphone
                                                                </label>
                                                                <input
                                                                    name="telephone"
                                                                    onChange={
                                                                        handleChange
                                                                    }
                                                                    value={
                                                                        participant.telephone
                                                                    }
                                                                    type="tel"
                                                                    className="form-control"
                                                                    id="exampleInputTel"
                                                                    aria-describedby="Tel"
                                                                />
                                                            </div>
                                                            <h1>
                                                                {evenement.id}
                                                            </h1>
                                                            <input
                                                                name="id_evenement"
                                                                value={
                                                                    "/api/evenements/" +
                                                                    evenement.id
                                                                }
                                                                // type="hidden"
                                                            />
                                                        </form>
                                                    </div>
                                                    <div className="modal-footer">
                                                        <button
                                                            type="button"
                                                            className="btn btn-secondary"
                                                            data-bs-dismiss="modal"
                                                        >
                                                            Retour
                                                        </button>
                                                        <button
                                                            onClick={
                                                                handleSubmit
                                                            }
                                                            type="submit"
                                                            className="btn btn-primary"
                                                        >
                                                            Valider
                                                            l'inscription
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {/* FIN MODALE */}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                );
            })}
        </div>
    );
}
