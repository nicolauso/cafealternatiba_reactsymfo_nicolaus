export default function Navbar() {
    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <a className="col-3 text-center" href="#">
                <img src="/images/logo.png" alt="" className="col-2" />
            </a>
            <div className="container-fluid">
                <button
                    className="navbar-toggler"
                    type="button"
                    data-bs-toggle="collapse"
                    data-bs-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent"
                    aria-expanded="false"
                    aria-label="Toggle navigation"
                >
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div
                    className="collapse navbar-collapse"
                    id="navbarSupportedContent"
                >
                    <ul className="navbar-nav me-auto mb-2 mb-lg-0 grid">
                        <li className="nav-item px-3">
                            <a
                                className="nav-link active"
                                aria-current="page"
                                href="/"
                            >
                                Accueil
                            </a>
                        </li>
                        <li className="nav-item px-3">
                            <a className="nav-link" href="/evenement">
                                Nos événements
                            </a>
                        </li>
                        <li className="nav-item px-3">
                            <a className="nav-link" href="/ajout">
                                Ajouter un événement
                            </a>
                        </li>
                        <li className="nav-item px-3">
                            <a className="nav-link" href="/contact">
                                Contact
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    );
}
