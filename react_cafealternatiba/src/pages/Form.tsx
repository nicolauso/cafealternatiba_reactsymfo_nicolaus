import Navbar from "../components/Navbar";
import Footer from "../components/Footer";

import { useState, useEffect } from "react";
import Evenement from "./Evenement";

interface Evenement {}

export default function Form() {
    const [evenement, setEvenement]: any = useState({});
    const [dataForm, setDataForm] = useState({
        intitule: "",
        theme: "",
        description: "",
        photo: [],
        participants: [],
    });

    const handleChange = (e) => {
        const { name, value } = e.target;
        setDataForm((prev) => {
            return {
                ...prev,
                [name]: value,
            };
        });
    };
    const handleSubmit = (e) => {
        e.preventDefault();

        fetch("http://127.0.0.1:8000/api/evenements", {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            body: JSON.stringify(dataForm),
        }).then(() => {
            alert("Votre événement a bien été enregistré");
            location.assign("http://localhost:5173/");
        });
    };

    return (
        <div>
            <nav>
                <Navbar />
            </nav>
            <main>
                <h2 className="p-4 ms-5">Ajouter un événement</h2>
                <form className="container mt-5 d-flex justify-content-center">
                    <div>
                        <div className="mb-3">
                            <label
                                htmlFor="exampleInputIntitule"
                                className="form-label"
                            >
                                Intitulé de l'événement
                            </label>
                            <input
                                name="intitule"
                                onChange={handleChange}
                                value={evenement.intitule}
                                type="intitule"
                                className="form-control"
                                id="exampleInputIntitule"
                                aria-describedby="intitule"
                            />
                        </div>
                        <div className="mb-3">
                            <label
                                htmlFor="exampleInputTheme"
                                className="form-label"
                            >
                                Thème
                            </label>
                            <input
                                name="theme"
                                onChange={handleChange}
                                value={evenement.theme}
                                type="theme"
                                className="form-control"
                                id="exampleInputTheme"
                                aria-describedby="theme"
                            />
                        </div>

                        <div className="mb-3">
                            <div className="form-group">
                                <label htmlFor="exampleFormControlTextarea1">
                                    Description de l'événement
                                </label>
                                <textarea
                                    name="description"
                                    onChange={handleChange}
                                    value={evenement.description}
                                    className="form-control"
                                    id="exampleFormControlTextarea1"
                                ></textarea>
                            </div>
                        </div>

                        <div className="mb-3">
                            <label htmlFor="formFile" className="form-label">
                                Ajouter une photo
                            </label>
                            <input
                                name="photo"
                                onChange={handleChange}
                                className="form-control"
                                type="file"
                                id="formFile"
                            />
                        </div>

                        <button
                            onClick={handleSubmit}
                            name="submit"
                            className="btn btn-primary my-4"
                        >
                            Valider
                        </button>
                    </div>
                </form>
            </main>
            <footer>
                <Footer />
            </footer>
        </div>
    );
}
