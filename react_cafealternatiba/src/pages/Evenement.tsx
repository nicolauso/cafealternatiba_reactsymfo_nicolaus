import Navbar from "../components/Navbar";
import Footer from "../components/Footer";
import OneEvent from "../components/OneEvent";

export default function Evenement() {
    return (
        <div>
            <header>
                <nav>
                    <Navbar />
                </nav>
            </header>
            <main className="container mt-5">
                <h1>Nos événements</h1>
                <OneEvent />
            </main>

            <footer>
                <Footer />
            </footer>
        </div>
    );
}
