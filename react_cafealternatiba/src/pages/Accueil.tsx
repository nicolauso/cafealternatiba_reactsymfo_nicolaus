import Navbar from "../components/Navbar";
import Footer from "../components/Footer";
import { useContext, useState, useEffect } from "react";
import { EvenementsContext } from "../EvenementsContext";

export default function Accuei() {
    const { evenements }: any = useContext(EvenementsContext);
    const derniersEvenements = evenements.slice(-3).reverse();

    useEffect(() => {}, [evenements]);
    return (
        <div>
            <header>
                <Navbar />
                <div className="position-relative bg-image vh-100 vw-100">
                    <div>
                        <h1 className="titreAccueil z-1 position-absolute top-50 start-50 translate-middle text-white">
                            CAFÉ ALTERNATIBA,
                        </h1>
                        <h2 className="titreAccueil z-1 position-absolute ms-5 mt-5 top-50 start-50 translate-middle text-white">
                            un lieu de rencontres pour un monde engagé
                        </h2>
                    </div>
                </div>
            </header>
            <main className="mb-5">
                <h2 className="p-5">Nos événements à la une</h2>
                <div className="container mb-5">
                    {/* LAST EVENTS */}
                    <div className="row mb-5">
                        {evenements &&
                            evenements
                                .slice(-3)
                                .map((evenement: any, index: number) => (
                                    <div
                                        className="card text-white col-md-4"
                                        key={index}
                                    >
                                        <div>
                                            <img
                                                src="/images/activite1.jpg"
                                                className="card-img"
                                                alt="acitivite"
                                            />
                                        </div>
                                        <div className="card-img-overlay d-flex flex-column align-items-center justify-content-center">
                                            <div className="bg-dark p-5 bg-opacity-50">
                                                <h5 className="card-title">
                                                    {evenement.intitule}
                                                </h5>
                                                <h6>mardi 12 juin à 15h</h6>
                                            </div>
                                        </div>
                                    </div>
                                ))}
                    </div>
                    {/* END LAST EVENTS */}
                    <a className="btn btn-primary" href="/evenement">
                        Voir plus
                    </a>
                </div>
                <section className="add p-5">
                    <div className="container text-center">
                        <h2>Toi aussi, tu peux proposer un événement !</h2>
                        <p className="py-4 d-flex justify-content-center">
                            Notre café est associatif, il accueille tout type
                            d'événement. Tu as envie de participer à la vie
                            associative de ta communauté en proposant un
                            événement culturel, artistique ou bricolage ? C'est
                            super ! N'hésite pas à nous partager tes idées, on
                            serait ravis de t'aider à les concrétiser. Ensemble,
                            on peut créer quelque chose de génial !
                        </p>
                        <a className="btn btn-primary" href="/ajout">
                            Ajouter un événement
                        </a>
                    </div>
                </section>
            </main>
            <Footer />
        </div>
    );
}
