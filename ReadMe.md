# README DU PROJET CAFE ALTERNATIBA

## PRESENTATION DU PROJET

Ce site est celui d'un café associatif, Café Alternatiba. Sur ce site, il est possible de :
- Voir tous les événements qui auront lieu au Café
- Ajouter un nouvel évenement

Les prochaines MAJ permettront de :
- Cliquer sur un événement depuis la page d'accueil et voir ses détails
- S'inscrire à l'événement souhaité

### Les technologies utilisées
- Front-end : ReactJS
- Back-end : Symfony, APIPlateform

## INSTALLATION ET PRISE EN MAIN

### CLONER LE REPOSITORY GIT
- Ouvre un terminal 
- Colle ce code : git@gitlab.com:nicolauso/cafealternatiba_reactsymfo_nicolaus.git

### INSTALLER LES DÉPENDANCES 
Installe les dépendances du projet :

- Ouvre un terminal dans le dossier React
```npm install```

- Ouvre un terminal dans le dossier Symfony
```composer install``` 


### CONFIGURATION : .ENV

Pour exécuter le projet, configure les variables d'environnement. Vous pouvez le faire en créant un fichier .env.local à la racine du projet et en y ajoutant les variables suivantes:


DATABASE_URL=sqlite:///%kernel.project_dir%/var/data.db
APP_ENV=dev
APP_SECRET=<your-secret-key>

Assurez-vous de remplacer <your-secret-key> par une chaîne aléatoire et sécurisée.

### EXECUTION 

Une fois que tu as installé et configuré le projet, tu peux l'exécuter en exécutant les commandes suivantes dans votre terminal:

php bin/console doctrine:database:create
php bin/console doctrine:schema:create
npm run dev

Ces commandes créent la base de données, créent le schéma de la base de données et lancent le serveur de développement.

### LANCER LES SERVERS

- Lance le serveur React dans un terminal depuis le dossier React avec `npm run dev`
- Lance le serveur Symfony dans un terminal depuis le dossier Symfony avec ``symfony server:start``

### NAVIGUER SUR LE SITE
- Clique sur le serveur de React
- Tu dois normalement pouvoir naviguer sur le port 


